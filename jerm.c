/*
 * jerm.c - terminal emulator
 * Copyright (c) 2000, 2001, 2002, 2003, 2004 KANDA Toshihiro
 *               2013, 2014 OBUCHI Yukio
 */
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <time.h>

#define PORT 8086

#define PI 3.1415926535897932

static char *myname;

enum parity_t {
	P_NONE,
	P_EVEN,
	P_ODD,
};

enum flow_t {
	F_NONE,
	F_HARD,
	F_X,
};

static int speed = B9600; /* B38400 - B75 */
static enum parity_t parity = P_NONE;
static int data_bit = CS8;
static int stop_bit = 1;
static enum flow_t flow_control = F_NONE;
static int hex_mode; /* hex dump モード */
static int quit_flag;
static char escape_character = '~';

/* 行末変換のプリミティブ */
enum rn_conv_t {
	RN_DROP, /* 捨てる */
	RN_MAP_CR, /* CR に変換 */
	RN_MAP_LF, /* LF に変換 */
	RN_MAP_CRLF, /* CR LF に変換 */
	RN_CONV_SIZE
};

/* rn_conv_t に対応する変換値 */
static char *RN_OUTPUT[RN_CONV_SIZE] = {
	"",
	"\r",
	"\n",
	"\r\n",
};

/* rn_conv_t に対応するオプション文字 */
static char RN_STR[RN_CONV_SIZE + 1] = "xrnt";

/* 行末変換テーブル rn_map[] のインデクス */
enum rn_index_t {
	RN_RECV_CR, /* リモートから CR 受信した */
	RN_RECV_LF, /* リモートから LF 受信した */
	RN_SEND_CR, /* ローカルから CR 受信した */
	RN_SEND_LF, /* ローカルから LF 受信した */
	RN_INDEX_SIZE
};

static enum rn_conv_t rn_map[RN_INDEX_SIZE] = {
	RN_MAP_CR,
	RN_MAP_LF,
	RN_MAP_CR,
	RN_MAP_LF,
};


/*
 * select on 2 file descriptors.
 */
static int
sel2(fd_set *m, int f1, int f2)
{
	int maxfd = f1 > f2 ? f1 : f2;
	FD_ZERO(m);
	FD_SET(f1, m);
	FD_SET(f2, m);
	errno = 0;
	return select(maxfd + 1, m, NULL, NULL, NULL);
}/* sel2 */

/*
 * ソケットに size_ バイト送信。
 */
static int
so_write(int so, const void *buf, size_t size_)
{
	int err = 0;
	int size = size_;
	const char *mv = buf;
	while (err == 0 && size >= 1) {
		int wrote = write(so, mv, size);
		if (wrote >= size) {
			size -= wrote;
			mv += wrote;
		}
		else
			err = -1;
	}/* while */
	if (err == 0)
		err = size_;
	return err;
}/* so_write */

/*
 *
 */
static struct termios tio_save;
static int ti_fd;

/*
 * 死ぬ前にターミナルの状態を戻す。
 */
static void
restore_local_terminal(void)
{
	tcsetattr(ti_fd, TCSANOW, &tio_save);
}/* restore_local_terminal */

/*
 * ターミナルの状態を保存。
 */
static int
save_local_terminal(int fd)
{
	int err = tcgetattr(fd, &tio_save);
	if (err < 0) {
		perror("tcgetattr");
	}
	else {
		ti_fd = fd;
		atexit(restore_local_terminal);
	}
	return err;
}/* save_local_terminal */


/*
 * cfmakeraw() by blogger323
 * <URL:http://blogger323.blog83.fc2.com/blog-entry-202.html>
 */
#ifdef __CYGWIN__ /* [ */
/* Workaround for Cygwin, which is missing cfmakeraw */
/* Pasted from man page; added in serial.c arbitrarily */
void
cfmakeraw(struct termios *termios_p)
{
	termios_p->c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP
	|INLCR|IGNCR|ICRNL|IXON);
	termios_p->c_oflag &= ~OPOST;
	termios_p->c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
	termios_p->c_cflag &= ~(CSIZE|PARENB);
	termios_p->c_cflag |= CS8;
}/* cfmakeraw */

#endif /* ] */

/*
 * ターミナルを raw モードにする。
 */
static int
make_local_raw(int fd)
{
	int err = 0;
	if (isatty(fd)) {
		err = save_local_terminal(fd);
		if (err == 0) {
			struct termios tio;
			if ((err = tcgetattr(fd, &tio)) < 0)
				perror("tcgetattr");
			else {
				cfmakeraw(&tio);
				if ((err = tcsetattr(fd, TCSANOW, &tio)) < 0)
					perror("tcsetattr");
			}
		}
	}
	return err;
}/* make_local_raw */

/*
 * 改行
 */
static char *NL = "\r\n";

/*
 * -r rnRN
 * 行末の変換オプション
 *    r: リモートから受信した CR をローカルに表示する時何に変換するか
 *    n: リモートから受信した NL をローカルに表示する時何に変換するか
 *    R: ローカルから受信した CR をリモートに送信する時何に変換するか
 *    N: ローカルから受信した NL をリモートに送信する時何に変換するか
 * それぞれ一桁の文字で表す。
 *    x = 受信した文字を捨てる。
 *    r = CR に変換する。
 *    n = LF に変換する。
 *    t = CR LF に変換する。
 *    - = 変換無し。
 */
static int
parse_rn_char(int c, int dflt)
{
	int ret = dflt;
	switch (c) {
	case 'x': ret = RN_DROP; break;
	case 'r': ret = RN_MAP_CR; break;
	case 'n': ret = RN_MAP_LF; break;
	case 't': ret = RN_MAP_CRLF; break;
	}/* switch */
	return ret;
}/* parse_rn_char */

static int
set_rn_opt(const char *rnopt)
{
	int err = -1;
	int len = strlen(rnopt);
	int rn[RN_INDEX_SIZE];
	if (len == RN_INDEX_SIZE) {
		if ((rn[RN_RECV_CR] = parse_rn_char(rnopt[RN_RECV_CR], -1)) != -1 &&
		    (rn[RN_RECV_LF] = parse_rn_char(rnopt[RN_RECV_LF], -1)) != -1 &&
		    (rn[RN_SEND_CR] = parse_rn_char(rnopt[RN_SEND_CR], -1)) != -1 &&
		    (rn[RN_SEND_LF] = parse_rn_char(rnopt[RN_SEND_LF], -1)) != -1) {
			memcpy(rn_map, rn, sizeof(rn_map));
			err = 0;
		}
	}
	return err;
}/* set_rn_opt */

static char *
print_rn_opt(char *buf, int size)
{
	char *ret = NULL;
	if (size >= RN_INDEX_SIZE + 1) {
		buf[RN_RECV_CR] = RN_STR[rn_map[RN_RECV_CR]];
		buf[RN_RECV_LF] = RN_STR[rn_map[RN_RECV_LF]];
		buf[RN_SEND_CR] = RN_STR[rn_map[RN_SEND_CR]];
		buf[RN_SEND_LF] = RN_STR[rn_map[RN_SEND_LF]];
		buf[RN_INDEX_SIZE] = '\0';
		ret = buf;
	}
	return ret;
}/* print_rn_opt */

/* 速度数値 と Bxxx定義値 の対応表 */
static struct speed_map {
	int num;
	speed_t speed;
} speed_map[] = {
	{ 0, B0 },
	{ 50, B50 },
	{ 75, B75 }, 
	{ 110, B110 },
	{ 134, B134 },
	{ 150, B150 },
	{ 200, B200 },
	{ 300, B300 },
	{ 600, B600 },
	{ 1200, B1200 },
	{ 1800, B1800 },
	{ 2400, B2400 },
	{ 4800, B4800 },
	{ 9600, B9600 },
	{ 19200, B19200 },
	{ 38400, B38400 },
	{ 57600, B57600 },
	{ 115200, B115200 },
};

/*
 * 速度数値 から Bxxx定義値に変換
 * だめならそのまま
 */
static int
speed_to_num(speed_t speed)
{
	int i = sizeof(speed_map)/sizeof(speed_map[0]);
	while(i--)
		if( speed_map[i].speed == speed)
			return speed_map[i].num;
	return speed;
}/* speed_to_num */

/*
 * Bxxx定義値 から 速度数値 定義値に変換
 * だめならそのまま
 */
static speed_t
num_to_speed(int num)
{
	int i = sizeof(speed_map)/sizeof(speed_map[0]);
	while(i--)
		if( speed_map[i].num == num)
			return speed_map[i].speed;
	return num;
}/* num_to_speed */


/*
 * ターミナルの状態を表示。
 * stty -a みたいなやつ。
 */
static void
print_status(const struct termios *tio)
{
	int cs;
	char *csstr = "?";
	speed_t ispeed = cfgetispeed(tio);
	speed_t ospeed = cfgetospeed(tio);
	fprintf(stderr, " ispeed %d", speed_to_num(ispeed));
	fprintf(stderr, " ospeed %d", speed_to_num(ospeed));
	fprintf(stderr, "%s", NL);
	fprintf(stderr, " %cIGNBRK", (tio->c_iflag & IGNBRK) ? '+' : '-');
	fprintf(stderr, " %cBRKINT", (tio->c_iflag & BRKINT) ? '+' : '-');
	fprintf(stderr, " %cIGNPAR", (tio->c_iflag & IGNPAR) ? '+' : '-');
	fprintf(stderr, " %cPARMRK", (tio->c_iflag & PARMRK) ? '+' : '-');
	fprintf(stderr, " %cINPCK", (tio->c_iflag & INPCK) ? '+' : '-');
	fprintf(stderr, " %cISTRIP", (tio->c_iflag & ISTRIP) ? '+' : '-');
	fprintf(stderr, " %cINLCR", (tio->c_iflag & INLCR) ? '+' : '-');
	fprintf(stderr, " %cIGNCR", (tio->c_iflag & IGNCR) ? '+' : '-');
	fprintf(stderr, " %cICRNL", (tio->c_iflag & ICRNL) ? '+' : '-');
	fprintf(stderr, " %cIXON", (tio->c_iflag & IXON) ? '+' : '-');
	fprintf(stderr, " %cIXOFF", (tio->c_iflag & IXOFF) ? '+' : '-');
	fprintf(stderr, " %cIXANY", (tio->c_iflag & IXANY) ? '+' : '-');
	fprintf(stderr, " %cIMAXBEL", (tio->c_iflag & IMAXBEL) ? '+' : '-');
	fprintf(stderr, "%s", NL);
	fprintf(stderr, " %cOPOST", (tio->c_oflag & OPOST) ? '+' : '-');
	fprintf(stderr, " %cONLCR", (tio->c_oflag & ONLCR) ? '+' : '-');
#ifdef OXTABS
	fprintf(stderr, " %cOXTABS", (tio->c_oflag & OXTABS) ? '+' : '-');
#endif
#if defined(TABDLY) && defined(XTABS) /* linux */
	fprintf(stderr, " %cTABDLY", (tio->c_oflag & TABDLY) == XTABS ? '+' : '-');
#endif
#ifdef ONOEOT
	fprintf(stderr, " %cONOEOT", (tio->c_oflag & ONOEOT) ? '+' : '-');
#endif
	fprintf(stderr, "%s", NL);
	cs = tio->c_cflag & CSIZE;
	switch (cs) {
	case CS5: csstr = "5"; break;
	case CS6: csstr = "6"; break;
	case CS7: csstr = "7"; break;
	case CS8: csstr = "8"; break;
	default: csstr = "?"; break;
	}/* switch */
	fprintf(stderr, " cs%s", csstr);
	fprintf(stderr, " %cCSTOPB", (tio->c_cflag & CSTOPB) ? '+' : '-');
	fprintf(stderr, " %cCREAD", (tio->c_cflag & CREAD) ? '+' : '-');
	fprintf(stderr, " %cPARENB", (tio->c_cflag & PARENB) ? '+' : '-');
	fprintf(stderr, " %cPARODD", (tio->c_cflag & PARODD) ? '+' : '-');
	fprintf(stderr, " %cHUPCL", (tio->c_cflag & HUPCL) ? '+' : '-');
	fprintf(stderr, " %cCLOCAL", (tio->c_cflag & CLOCAL) ? '+' : '-');
#ifdef CCTS_OFLOW
	fprintf(stderr, " %cCCTS_OFLOW", (tio->c_cflag & CCTS_OFLOW) ? '+' : '-');
#endif
	fprintf(stderr, " %cCRTSCTS", (tio->c_cflag & CRTSCTS) ? '+' : '-');
#ifdef CRTS_IFLOW
	fprintf(stderr, " %cCRTS_IFLOW", (tio->c_cflag & CRTS_IFLOW) ? '+' : '-');
#endif
#ifdef MDMBUF
	fprintf(stderr, " %cMDMBUF", (tio->c_cflag & MDMBUF) ? '+' : '-');
#endif
	fprintf(stderr, " %cECHOKE", (tio->c_lflag & ECHOKE) ? '+' : '-');
	fprintf(stderr, " %cECHOE", (tio->c_lflag & ECHOE) ? '+' : '-');
	fprintf(stderr, " %cECHO", (tio->c_lflag & ECHO) ? '+' : '-');
	fprintf(stderr, " %cECHONL", (tio->c_lflag & ECHONL) ? '+' : '-');
#ifdef ECHOPRT
	fprintf(stderr, " %cECHOPRT", (tio->c_lflag & ECHOPRT) ? '+' : '-');
#endif
	fprintf(stderr, " %cECHOCTL", (tio->c_lflag & ECHOCTL) ? '+' : '-');
	fprintf(stderr, " %cISIG", (tio->c_lflag & ISIG) ? '+' : '-');
	fprintf(stderr, " %cICANON", (tio->c_lflag & ICANON) ? '+' : '-');
#ifdef ALTWERASE
	fprintf(stderr, " %cALTWERASE", (tio->c_lflag & ALTWERASE) ? '+' : '-');
#endif
	fprintf(stderr, " %cIEXTEN", (tio->c_lflag & IEXTEN) ? '+' : '-');
	fprintf(stderr, "%s", NL);
#ifdef EXTPROC
	fprintf(stderr, " %cEXTPROC", (tio->c_lflag & EXTPROC) ? '+' : '-');
#endif
	fprintf(stderr, " %cTOSTOP", (tio->c_lflag & TOSTOP) ? '+' : '-');
	fprintf(stderr, " %cFLUSHO", (tio->c_lflag & FLUSHO) ? '+' : '-');
#ifdef NOKERNINFO
	fprintf(stderr, " %cNOKERNINFO", (tio->c_lflag & NOKERNINFO) ? '+' : '-');
#endif
#ifdef PENDIN
	fprintf(stderr, " %cPENDIN", (tio->c_lflag & PENDIN) ? '+' : '-');
#endif
	fprintf(stderr, " %cNOFLSH", (tio->c_lflag & NOFLSH) ? '+' : '-');
	fprintf(stderr, "%s", NL);
	fflush(stderr);
}/* print_status */

/*
 * グローバル変数を参照して、ターミナルの状態
 * (speed, parity, data bits 等)を設定する。
 */
static void
set_status(struct termios *tio)
{
	cfsetispeed(tio, speed);
	cfsetospeed(tio, speed);
	tio->c_cflag = (tio->c_cflag & ~CSIZE) | data_bit;
	if (stop_bit == 1)
		tio->c_cflag &= ~CSTOPB;
	else if (stop_bit == 2)
		tio->c_cflag |= CSTOPB;
	switch (parity) {
	case P_NONE:
		tio->c_cflag &= ~PARENB;
		break;
	case P_EVEN:
		tio->c_cflag = (tio->c_cflag | PARENB) & ~PARODD;
		break;
	case P_ODD:
		tio->c_cflag = tio->c_cflag | PARENB | PARODD;
		break;
	}/* switch */
	switch (flow_control) {
	case F_NONE:
		tio->c_cflag &= ~CRTSCTS;
		tio->c_iflag &= ~(IXON | IXOFF);
		break;
	case F_HARD:
		tio->c_cflag |= CRTSCTS;
		tio->c_iflag &= ~(IXON | IXOFF);
		break;
	case F_X:
		tio->c_cflag &= ~CRTSCTS;
		tio->c_iflag |= (IXON | IXOFF);
		break;
	}/* switch */
}/* set_status */

/*
 * 指定されたファイルハンドルのターミナル状態を表示。
 */
static int
print_fdstatus(int fd)
{
	int err = -1;
	struct termios tio;
	if (tcgetattr(fd, &tio) < 0)
		perror("tcgetattr");
	else {
		print_status(&tio);
		err = 0;
	}
	return err;
}/* print_fdstatus */

/*
 * 指定されたファイルハンドルのターミナルの状態を初期化する。
 * raw モードにして、CLOCAL を設定する。
 */
static int
fd_init(int fd)
{
	struct termios tio;
	int err = -1;
	if (tcgetattr(fd, &tio) < 0)
		perror("tcgetattr");
	else {
		cfmakeraw(&tio);
		tio.c_cflag |= CLOCAL; /* do not send SIGHUP to me! */
		if (tcsetattr(fd, TCSANOW, &tio) < 0)
			perror("tcsetattr");
		else
			err = 0;
	}
	return err;
}/* fd_init */

/*
 * 指定されたファイルハンドルについて、
 * グローバル変数を参照して、ターミナルの状態
 * (speed, parity, data bits 等)を設定する。
 */
static int
set_fdstatus(int fd)
{
	struct termios tio;
	int err = -1;
	if (tcgetattr(fd, &tio) < 0)
		perror("tcgetattr");
	else {
		set_status(&tio);
		if (tcsetattr(fd, TCSANOW, &tio) < 0)
			perror("tcsetattr");
		else
			err = 0;
	}
	return err;
}/* set_fdstatus */

/*
 *
 */

static int
chardump(const void *buf_, size_t size)
{
	const unsigned char *buf = buf_;
	int i;
	printf(" |");
	for (i = 0; i < size; i++) {
		if (buf[i] >= ' ' && buf[i] < 0x7f)
			printf("%c", buf[i]);
		else
			printf(".");
	}/* for */
	printf("%s", NL);
	fflush(stdout);
	return 0;
}/* chardump */

static unsigned char hex_buf[16];
static int hex_count;

static int
hexdump_start(void)
{
	fprintf(stderr, "[HEX MODE]%s", NL);
	hex_count = 0;
	return 0;
}/* hexdump_start */

static int
hexdump_stop(void)
{
	if (hex_count > 0)
		chardump(hex_buf, hex_count);
	fprintf(stderr, "[NORMAL MODE]%s", NL);
	return 0;
}/* hexdump_stop */

/*
 *
 */
static int
hexdump(const void *buf_, size_t size)
{
	const unsigned char *buf = buf_;
	int i;
	for (i = 0; i < size; i++) {
		printf("%02x ", buf[i]);
		hex_buf[hex_count++] = buf[i];
		if (hex_count == sizeof(hex_buf)) {
			chardump(hex_buf, hex_count);
			hex_count = 0;
		}
	}/* for */
	return 0;
}/* hexdump */

/*
 * リモートからデータが送られて来た処理。
 */
static int
read_remote(int localfd, int remotefd, FILE *log)
{
	unsigned char lbuf[80];
	int err = read(remotefd, lbuf, sizeof(lbuf));
	if (err < 0)
		perror("remote read");
	else if (err == 0) {
		fprintf(stderr, "remote closed");
		err = -1;
	}
	else {
		if (log != NULL) {
			fwrite(lbuf, 1, err, log);
			fflush(log);
		}
		else if (hex_mode)
			err = hexdump(lbuf, err);
		else {
			int i = 0, n = err;
			err = 0;
			while (err == 0 && i < n) {
				switch (lbuf[i]) {
				case '\n':	
					if (fputs(RN_OUTPUT[rn_map[RN_RECV_LF]], stdout) == EOF)
						err = -1;
					break;
				case '\r':
					if (fputs(RN_OUTPUT[rn_map[RN_RECV_CR]], stdout) == EOF)
						err = -1;
					break;
				default:
					if (fputc(lbuf[i], stdout) == EOF)
						err = -1;
				}/* switch */
				i++;
			}/* while */
		}
		fflush(stdout);
	}
	return err;
}/* read_remote */

/*
 *
 */
static char *
read_line(char *buf, int size, int fd)
{
	int idx = 0, err = 0, done = 0, ch = EOF;
	char *ret = NULL, lbuf[1];
	while (!done && (err = read(fd, lbuf, 1) > 0)) {
		int i;
		ch = (unsigned char)lbuf[0];
		switch (ch) {
		case '[' - '@':
			fprintf(stderr, "%s", NL);
			done = 1;
			break;
		case 'M' - '@':
		case 'J' - '@':
			if (idx < size)
				buf[idx] = '\0';
			fprintf(stderr, "%s", NL);
			done = 1;
			break;
		case 'H' - '@':
			if (idx >= 1) {
				fprintf(stderr, "\b \b");
				idx--;
			}
			break;
		case 'U' - '@':
			for (i = 0; i < idx; i++)
				fprintf(stderr, "\b \b");
			idx = 0;
			break;
		default:
			if (iscntrl(ch))
				fprintf(stderr, "\a");
			else {
				if (idx + 1 < size) { /* keep 1 for '\0' */
					buf[idx++] = ch;
					fprintf(stderr, "%c", ch);
				}
				else
					fprintf(stderr, "\a");
			}
			break;
		}/* switch */
	}/* while */
	if (err > 0) {
		if (ch != '[' - '@')
			ret = buf;
	}
	return ret;
}/* read_line */

/*
 *
 */
static int
put_file(const char *name, int fd)
{
	int err = -1;
	FILE *fp = fopen(name, "r");
	if (fp == NULL)
		fprintf(stderr, "%s: %s%s", name, strerror(errno), NL);
	else {
		int xx;
		char lbuf[256];
		err = 0;
		while (err == 0 && (xx = fread(lbuf, 1, sizeof(lbuf), fp)) > 0) {
			if (write(fd, lbuf, xx) != xx) {
				fprintf(stderr, "%s: %s%s", name, strerror(errno), NL);
				err = -1;
			}
		}/* while */
		fclose(fp);
	}
	return err;
}/* put_file */

/*
 * エスケープ (改行・チルダ) の後に cmd が入力された処理。
 * return
 *	0: ok (エスケープ続く)
 *	1: ok (エスケープ終る)
 *	-1: error (エスケープ終る)
 */
static int
do_escape(int cmd, int localfd, int remotefd)
{
	int err = 0;
	char obuf[2];
	switch (cmd) {
	case '?':
		fprintf(stderr, "[COMMANDS]%s", NL);
        fprintf(stderr, "[%c?]\thelp%s", escape_character, NL);
		fprintf(stderr, "[%c.]\tquit%s", escape_character, NL);
		fprintf(stderr, "[%c%c]\tsend '%c'%s", escape_character, escape_character, escape_character, NL);
		fprintf(stderr, "[%c#]\tsend break%s", escape_character, NL);
		fprintf(stderr, "[%cx]\ttoggle hex mode%s", escape_character, NL);
		fprintf(stderr, "[%cr rnRN]\tset crlf map mode%s", escape_character, NL);
		fprintf(stderr, "[%c>flie]\tput file%s", escape_character, NL);
		break;
	case '.':
		quit_flag = 1; /* プログラム終了 */
		break;
	case '>':
		{
			char lbuf[256];
			fprintf(stderr, "put file>");
			if (read_line(lbuf, sizeof(lbuf), localfd) != NULL) {
				put_file(lbuf, remotefd);
			}
		}
		break;	
	case '#':
		if (tcsendbreak(remotefd, 0) < 0) {
			perror("tcsendbreak");
			err = -1;
		}
		break;
	case 'x':
		hex_mode = !hex_mode;
		if (hex_mode)
			hexdump_start();
		else
			hexdump_stop();
		break;
	case 'r':
		{
			char lbuf[8], map[8];
			print_rn_opt(map, sizeof(map));
			fprintf(stderr, "crlf map: x=drop, r=CR, n=LF, t=CRLF%s", NL);
			fprintf(stderr, "crlf map: option order = {recv CR, recv NL, send CR, send LF}%s", NL);
			fprintf(stderr, "crlf map: current option=%s, input new option=", map);
			if (read_line(lbuf, sizeof(lbuf), localfd) != NULL) {
				if (set_rn_opt(lbuf) < 0)
					fprintf(stderr, "%s: wrong format. ignored%s", lbuf, NL);
			}
		}
		break;
	default:
        if( cmd == escape_character ) {
            if (so_write(remotefd, &escape_character, 1) != 1) {
                perror("write remote");
                err = -1;
            }
        } else {
            err = 1;
            obuf[0] = escape_character;
            obuf[1] = cmd;
            if (so_write(remotefd, obuf, 2) != 2) {
                perror("write remote");
                err = -1;
            }
        }
		break;
	}/* switch */
	return err;
}/* do_escape */

/*
 * キーボードから入力があった処理。
 */
static int
read_local(int localfd, int remotefd, FILE *log)
{
	static int lastch = '\n';
	unsigned char lbuf[1];
	int err = read(localfd, lbuf, sizeof(lbuf));
	if (err < 0)
		perror("local read");
	else if (err == 0) {
		fprintf(stderr, "local closed\n");
		err = -1;
	}
	else {
		if ((lastch == '\n' || lastch == '\r') && lbuf[0] == escape_character ) {
			err = read(localfd, lbuf, sizeof(lbuf));
			if (err < 0)
				perror("local read");
			else if (err == 0) {
				fprintf(stderr, "local closed\n");
				err = -1;
			}
			else {
				err = do_escape(lbuf[0], localfd, remotefd);
				if (err != 0)
					lastch = lbuf[0];
				if (err == 1)
					err = 0;
			}
		}
		else {
			char *obuf = (char *)lbuf;
			int n = err;
			switch (lbuf[0]) {
			case '\r':
				obuf = RN_OUTPUT[rn_map[RN_SEND_CR]];
				n = strlen(obuf);
				break;
			case '\n':
				obuf = RN_OUTPUT[rn_map[RN_SEND_LF]];
				n = strlen(obuf);
				break;
			}/* switch */
			err = so_write(remotefd, obuf, n);
			if (err < 0) {
				perror("write remote");
			}
			else
				err = 0;
			lastch = lbuf[0];
		}
	}
	return err;
}/* read_local */

/*
 * メインループ
 */
static int
fnain(int localfd, int remotefd, FILE *log)
{
	int err = 0;
	quit_flag = 0;
	while (quit_flag == 0 && err == 0) {
		fd_set fds;
		err = sel2(&fds, localfd, remotefd);
		if (err == -1)
			perror("select");
		else if (err == 0) {
			/* timeout */
		}
		else {
			if (FD_ISSET(remotefd, &fds)) {
				err = read_remote(localfd, remotefd, log);
			}
			if (FD_ISSET(localfd, &fds)) {
				err = read_local(localfd, remotefd, log);
			}
		}
	}/* while */
	return err;
}/* fnain */

/*
 * ファイル名 dev を通してシリアル通信を行う。
 */
static int
nain(int localfd, const char *dev, FILE *log)
{
	int err = -1;
	int remotefd = open(dev, O_RDWR);
	if (remotefd < 0)
		perror(dev);
	else {
		if (make_local_raw(localfd) == 0 && fd_init(remotefd) == 0 && set_fdstatus(remotefd) == 0) {
            print_fdstatus(remotefd);
			err = fnain(localfd, remotefd, log);
		}
		close(remotefd);
	}
	return err;
}/* nain */

static char *copyright = "Jerminal v0.8096-Simplified  \n\tCopyright (C) 2000, 2001, 2002, 2003, 2004, 2005, 2007 candy \n\t\t2013, 2014 buchio";

/*
 *
 */
static char *usage_msg =
	"usage1: %s [optinos] device\n"
	"\t-b speed\tspeed\n"
	"\t-p [none|even|odd]\tparity\n"
	"\t-d [7|8]\tdata bits\n"
	"\t-s [1|2|1.5]\tstop bit\n"
	"\t-f [none|x|hard]\tflow control\n"
	"\t-e escape character\n"
	"\t-l file\tlog file\n"
	"\t-z\ttruncate log file\n"
	"\t-x\tstart in hex dump mode\n"
	"\t-r cccc\tset CR LF mapping. (default = rnrn)\n"
	"\t\tc = {x=drop, r=CR, n=LF, t=CRLF}\n"
	"\t\toption order = {recv CR, recv NL, send CR, send LF}\n"
	;

/*
 *
 */
int
main(int argc, char *argv[])
{
	int ex = 1, ch, show_usage = 0;
	int truncate_log = 0;
	int family = AF_INET; /* AF_UNSPEC; */
	char *logfile = NULL;
	int daemon = 0;
	myname = argv[0];
	while ((ch = getopt(argc, argv, "46b:d:Df:ijl:e:p:P:r:s:TVxz")) != EOF) {
		switch (ch) {
			int x;
		default:
		case 'V':
			show_usage = 1;
			break;
		case '4':
			family = AF_INET;
			break;
		case '6':
#ifdef AF_INET6
			family = AF_INET6;
#else
			fprintf(stderr, "%s: WARNING: IPv6 is not supported, ignored.\n", myname);
#endif
			break;
		case 'b':
			x = strtol(optarg, NULL, 10);
			speed = num_to_speed(x);
			break;
		case 'd':
			x = strtol(optarg, NULL, 10);
			switch (x) {
			case 8: data_bit = CS8; break;
			case 7: data_bit = CS7; break;
			default: show_usage = 1; break;
			}/* switch */
			break;
		case 'D':
			daemon++;
			break;
		case 'f':
			switch (optarg[0]) {
			case 'n': flow_control = F_NONE; break;
			case 'x': flow_control = F_X; break;
			case 'h': flow_control = F_HARD; break;
			default: show_usage = 1; break;
			}/* switch */
			break;
		case 'e':
			escape_character = optarg[0];
			break;
		case 'l':
			logfile = optarg;
			break;
		case 'p':
			switch (optarg[0]) {
			case 'n': parity = P_NONE; break;
			case 'e': parity = P_EVEN; break;
			case 'o': parity = P_ODD; break;
			default: show_usage = 1; break;
			}/* switch */
			break;
		case 'r':
			if (set_rn_opt(optarg) < 0) {
				fprintf(stderr, "%s: %s: wrong format\n", myname, optarg);
				show_usage++;
			}
			break;
		case 's':
			if (strcmp(optarg, "1") == 0)
				stop_bit = 1;
			else if (strcmp(optarg, "2") == 0)
				stop_bit = 2;
			else if (strcmp(optarg, "1.5") == 0)
				stop_bit = 3;
			else	
				show_usage = 1;
			break;
		case 'x':
			hex_mode = 1;
			break;
		case 'z':
			truncate_log = 1;
			break;
		}/* switch */
	}/* while */
	if (argc - optind != 1)
		show_usage++;
	if (show_usage) {
		fprintf(stderr, "%s\n", copyright);
		fprintf(stderr, usage_msg, myname, myname, myname);
	}
	else {
		int err = 0;
		FILE *logfp = NULL;
		if (logfile != NULL) {
			logfp = fopen(logfile, (truncate_log ? "w" : "a"));
			if (logfp == NULL) {
				perror(logfile);
				err = -1;
			}
		}
		if (err == 0) {
			if (hex_mode)
				hexdump_start();
            fprintf(stderr, "%s\n", copyright);
            fprintf(stderr, "Type \"Ctrl-M %c .\" to exit.\n", escape_character );
            nain(0, argv[optind], logfp);
			ex = 0;
		}
	}
	return ex;
}/* main */
