# $Id: Makefile,v 1.17 2007/08/09 08:35:12 candy Exp $
#

SRCS=jerm.c
OBJS=jerm.o
CFLAGS=-g -pipe -Wall
LDLIBS=-lm

SRCS=jerm.c
OBJS=jerm.o

all: jerm

jerm: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LDLIBS)

clean:
	rm -f $(OBJS) jerm

V=jerm-8096-simplefied
FILES=README Makefile $(SRCS) jerm.1
dist:
	rm -rf $(V) && \
	mkdir $(V) && \
	ln $(FILES) $(V) && \
	tar -czf $(V).tar.gz $(V) && \
	rm -rf $(V)
